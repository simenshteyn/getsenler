import asyncio
from aiohttp import ClientSession
import re
import time
import motor.motor_asyncio
DB_NAME = 'senler'

async def get_away_link(link_id, semaphore, session):
    await semaphore.acquire()
    print('Process started for {} id'.format(link_id))
    start = time.time()
    senler_url = 'https://senler.ru/away/{}/1'.format(link_id)
    try:
        async with session.get(senler_url) as resp:
            if resp.status in [200, 301]:
                if str(resp.url) == 'https://senler.ru/':
                   semaphore.release()

                elif str(resp.url) in ('https://away.vk.com/away.php', 'http://away.vk.com/away.php'):
                    uri = await url_in_string(str(await resp.text()))
                    result = await save_to_db(link_id, uri[0])
                    semaphore.release()
                    return print('ID {} (away site): {}'.format(link_id, uri[0]))
                elif str(resp.url) == senler_url:
                    uri = await url_in_string(str(await resp.text()))
                    result = await save_to_db(link_id, uri[0])
                    semaphore.release()
                    return print('ID {} (VK link): {}'.format(link_id, uri[0]))
                else:
                    result = await save_to_db(link_id, str(resp.url))
                    semaphore.release()
                    return print("ID {} (other link): {}".format(link_id, resp.url))
            else:
                semaphore.release()
    except:
        semaphore.release()
        return print("Some ERROR")

async def save_to_db(number, link):
    client = motor.motor_asyncio.AsyncIOMotorClient()
    db = client[DB_NAME]
    data = db['links3']
    document = {}
    document['_id'] = number
    document['url'] = str(link)
    result = await data.insert_one(document)

async def url_in_string(string):
    '''
        Find URL in string. Returns string
    '''
    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', str(string))
    return urls


async def asynchronous(a,b):
    start = time.time()
    async with ClientSession() as session:
        semaphore = asyncio.Semaphore(value=300)
        futures = [get_away_link(i, semaphore, session) for i in range(a,b)]
        done = await asyncio.wait(futures)
    print("Work took: {:.2f} seconds".format(time.time() - start))

loop = asyncio.get_event_loop()
loop.run_until_complete(asynchronous(1,700000))
loop.close()